#
# Be sure to run `pod lib lint ZunosCoreKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ZunosCoreKit"
  s.version          = "3.2.8"
  s.summary          = "A library version of Core for use in the Zunos iOS app."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = "Always wanted to run Core in Zunos without having to compile it every time? We know we have! So here is the solution."

  s.homepage         = "http://zunos.com"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Luke Johnson" => "luke@zunos.com" }
  s.source           = { :git => "git@bitbucket.org:arisapp/core-ios.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/zunos'

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*.{h,m,cpp,mm}'
  s.resource_bundles = {
    'ZunosCoreKit' => ["Pod/CoreData/**/*.xcdatamodeld",
                    "Pod/Resources/**/*.storyboard",
                    "Pod/Assets/**/*.png",
                    "Pod/Assets/**/*.json",
                    "Pod/Classes/**/*.xib"]
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit', 'AddressBook', 'MessageUI', 'StoreKit', 'CoreData', 'CoreText', 'QuartzCore', 'CoreGraphics', 'CoreFoundation', 'Foundation'
  s.dependency 'SBJson', '~> 3.2'
  s.dependency 'SDWebImage', '~> 3.7.1'
  s.dependency 'UIActivityIndicator-for-SDWebImage', '~> 1.2'
  s.dependency 'AFNetworking', '~> 2.5'
  s.dependency 'TYMActivityIndicatorView', '~> 0.2.0'
end
